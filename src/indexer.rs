use crate::model::parser::WordToId;
use std::collections::HashMap;

#[derive(Debug)]
pub struct Indexer {
    word_to_id_map: HashMap<String, u32>,
}

impl Indexer {
    pub fn new() -> Self {
        Indexer {
            word_to_id_map: HashMap::new(),
        }
    }

    pub fn get_id(&self, word: &str) -> Option<u32> {
        self.word_to_id_map.get(word).map(|id| *id)
    }
}

impl WordToId for Indexer {
    fn word_to_id(&mut self, word: &str) -> u32 {
        if let Some(present) = self.word_to_id_map.get(word) {
            *present
        } else {
            let id = self.word_to_id_map.len() as u32;
            self.word_to_id_map.insert(word.to_string(), id);
            id
        }
    }
}
