use itertools::Itertools;

pub fn high_is_better(scores: &mut [f64]) {
    let max = without_zeroes(scores).cloned().fold1(f64::max).unwrap();

    scores.iter_mut().for_each(|score| {
        // score could be zero here but that is ok
        *score = *score / max;
    });
}

pub fn low_is_better(scores: &mut [f64]) {
    let min = without_zeroes(scores).cloned().fold1(f64::min).unwrap();

    scores.iter_mut().for_each(|score| {
        // min can't be zero here but score could still be
        *score = min / f64::max(*score, std::f64::MIN_POSITIVE);
    });
}

fn without_zeroes(scores: &[f64]) -> impl Iterator<Item = &f64> {
    scores.iter().map(|score| {
        if *score > 0.0 {
            score
        } else {
            &std::f64::MIN_POSITIVE
        }
    })
}
